<?php

declare(strict_types=1);

namespace Drupal\seo_urls;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\seo_urls\Entity\SeoUrlInterface;
use Drupal\seo_urls\Form\EntityListFiltersForm;
use Drupal\system\Entity\Action;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a class to build a listing of SEO URL entities.
 *
 * @ingroup seo_urls
 */
class SeoUrlEntityListBuilder extends EntityListBuilder implements FormInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The key to use for the form element containing the entities.
   *
   * @var string
   */
  protected string $entitiesKey = 'entities';

  /**
   * The entities being listed.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected array $entities = [];

  /**
   * The bulk operations.
   *
   * @var \Drupal\system\Entity\Action[]
   */
  protected array $actions;

  /**
   * The action storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $actionStorage;

  /**
   * {@inheritDoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    $instance = parent::createInstance($container, $entity_type);
    $instance->formBuilder = $container->get(FormBuilderInterface::class);
    $instance->actionStorage = $container->get(EntityTypeManagerInterface::class)->getStorage('action');
    $instance->requestStack = $container->get(RequestStack::class);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return $this->entityTypeId . '_list';
  }

  /**
   * Implement filters from request parameters.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query to apply the filter to.
   */
  protected function applyFilters(QueryInterface $query): void {
    $filters = [
      SeoUrlInterface::CANONICAL_URL_FIELD,
      SeoUrlInterface::SEO_URL_FIELD,
    ];
    foreach ($filters as $filter) {
      if (NULL === ($request = $this->requestStack->getCurrentRequest())) {
        continue;
      }

      // Bail out on missing query parameter or reporting period.
      if (!$request->query->has($filter)) {
        continue;
      }

      $value = (string) $request->query->get($filter);
      $query->condition($filter, '%' . $value . '%', 'LIKE');
    }
  }

  /**
   * Returns a query object for loading entity IDs from the storage.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query object used to load entity IDs.
   */
  protected function getEntityListQuery(): QueryInterface {
    // Compared to parent method, do not include the default sorting on entity
    // id, as it prevents other sorting from working.
    $query = $this->getStorage()->getQuery()
      ->accessCheck();

    // Add a simple table sort by header, see ::buildHeader().
    $header = $this->buildHeader();
    $query->tableSort($header);

    // Add the filters.
    $this->applyFilters($query);

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [];
    $this->addHeader($header, $this->t('ID'), 'id');
    $this->addHeader($header, $this->t('Canonical URL'), SeoUrlInterface::CANONICAL_URL_FIELD);
    $this->addHeader($header, $this->t('SEO URL'), SeoUrlInterface::SEO_URL_FIELD);

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\seo_urls\Entity\SeoUrl $entity */
    $row['id']['#markup'] = $entity->id();
    $row['canonical_url'] = [
      'data' => [
        '#type' => 'link',
        '#title' => $entity->getCanonicalPath(),
        '#url' => Url::fromUri($entity->getCanonicalUri()),
        '#options' => ['attributes' => ['target' => '_blank']],
      ],
    ];
    $row['seo_url'] = [
      'data' => [
        '#type' => 'link',
        '#title' => $entity->getSeoPath(),
        '#url' => Url::fromUri($entity->getSeoUriBase()),
        '#options' => ['attributes' => ['target' => '_blank']],
      ],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * Add a new header cell to the provided headers.
   *
   * @param array $header
   *   The headers to attach the new cell to.
   * @param \Drupal\Component\Render\MarkupInterface|string $label
   *   The column's label.
   * @param string $machine_name
   *   The field's machine name.
   */
  public function addHeader(array &$header, MarkupInterface|string $label, string $machine_name): void {
    $header[$machine_name] = [
      'data' => $label,
      'specifier' => $machine_name,
      'field' => $machine_name,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    // Filter the actions to only include those for this entity type.
    $entity_type_id = $this->entityTypeId;
    /** @var \Drupal\system\Entity\Action[] $actions */
    $actions = $this->actionStorage->loadMultiple();
    $this->actions = array_filter($actions, function (Action $action) use ($entity_type_id) {
      return $action->getType() == $entity_type_id;
    });
    $this->entities = $this->load();
    if ($this->entities) {
      return $this->formBuilder->getForm($this);
    }

    $build['filters'] = $this->formBuilder->getForm(EntityListFiltersForm::class);
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#tableselect' => TRUE,
      '#attached' => [
        'library' => ['core/drupal.tableselect'],
      ],
    ];

    $this->entities = $this->load();
    foreach ($this->entities as $entity) {
      $form[$this->entitiesKey][$entity->id()] = $this->buildRow($entity);
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply to selected items'),
      '#button_type' => 'primary',
    ];

    // Ensure a consistent container for filters/operations in the view header.
    $form['header'] = [
      '#type' => 'container',
      '#weight' => -100,
    ];

    $action_options = [];
    foreach ($this->actions as $id => $action) {
      $action_options[$id] = $action->label();
    }
    $form['header']['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => $action_options,
    ];
    // Duplicate the form actions into the action container in the header.
    $form['header']['actions'] = $form['actions'];

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $form['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $selected = array_filter($form_state->getValue($this->entitiesKey));
    if (empty($selected)) {
      $form_state->setErrorByName($this->entitiesKey, $this->t('No items selected.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $selected = array_filter($form_state->getValue($this->entitiesKey));
    $entities = [];
    $action = $this->actions[$form_state->getValue('action')];
    $count = 0;

    foreach ($selected as $id) {
      $entity = $this->entities[$id];
      // Skip execution if the user did not have access.
      if (!$action->getPlugin()->access($entity)) {
        $this->messenger()->addError($this->t('No access to execute %action on the @entity_type_label %entity_label.', [
          '%action' => $action->label(),
          '@entity_type_label' => $entity->getEntityType()->getLabel(),
          '%entity_label' => $entity->label(),
        ]));
        continue;
      }

      $count++;
      $entities[$id] = $entity;
    }

    // Don't perform any action unless there are some elements affected.
    // @see https://www.drupal.org/project/drupal/issues/3018148
    if (!$count) {
      return;
    }

    $action->execute($entities);

    $operation_definition = $action->getPluginDefinition();
    if (!empty($operation_definition['confirm_form_route_name'])) {
      $options = [
        'query' => $this->getDestinationArray(),
      ];
      $form_state->setRedirect($operation_definition['confirm_form_route_name'], [], $options);
    }
    else {
      $this->messenger()->addStatus($this->formatPlural($count, '%action was applied to @count item.', '%action was applied to @count items.', [
        '%action' => $action->label(),
      ]));
    }
  }

}
