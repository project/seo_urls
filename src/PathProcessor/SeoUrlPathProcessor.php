<?php

declare(strict_types=1);

namespace Drupal\seo_urls\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\seo_urls\SeoUrlManager;
use Drupal\seo_urls\SeoUrlManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Processes the inbound path using SEO Url vocabulary.
 */
class SeoUrlPathProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * SEO url manager.
   *
   * @var \Drupal\seo_urls\SeoUrlManager|null
   */
  protected ?SeoUrlManager $seoUrlManager = NULL;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack|null
   */
  protected ?RequestStack $requestStack = NULL;

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected ContainerInterface $container;

  /**
   * Constructs a SeoUrlPathProcessor object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The current service container.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * Retrieve SEO URL manager service.
   *
   * @return \Drupal\seo_urls\SeoUrlManagerInterface
   *   SEO url manager.
   */
  public function getSeoUrlManager(): SeoUrlManagerInterface {
    // We shouldn't receive this service via dependency injection.
    // The services like current user, request stack always must be actual
    // to the moment when they are called.
    if (is_null($this->seoUrlManager)) {
      $this->seoUrlManager = $this->container->get(SeoUrlManagerInterface::class);
    }
    return $this->seoUrlManager;
  }

  /**
   * Get current request.
   *
   * @return \Symfony\Component\HttpFoundation\Request|null
   *   Current request.
   */
  public function getCurrentRequest(): ?Request {
    // We shouldn't receive this service via dependency injection.
    // The services like current user, request stack always must be actual
    // to the moment when they are called.
    if (is_null($this->requestStack)) {
      $this->requestStack = $this->container->get(RequestStack::class);
    }

    return $this->requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    return $this->getSeoUrlManager()->getCanonicalUrlBySeoUrl($path, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL): string {
    $langcode = isset($options['language']) ? $options['language']->getId() : NULL;
    $request = $request ?: $this->getCurrentRequest();
    if (!$request) {
      return $path;
    }

    $path = $this->getSeoUrlManager()->getSeoUrlByCanonicalUrl($path, $request, $langcode);
    // Ensure the resulting path has at most one leading slash, to prevent it
    // becoming an external URL without a protocol like //example.com. This
    // is done in \Drupal\Core\Routing\UrlGenerator::generateFromRoute()
    // also, to protect against this problem in arbitrary path processors,
    // but it is duplicated here to protect any other URL generation code
    // that might call this method separately.
    if (strpos($path, '//') === 0) {
      $path = '/' . ltrim($path, '/');
    }
    return $path;
  }

}
