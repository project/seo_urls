<?php

declare(strict_types=1);

namespace Drupal\seo_urls;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the class SEO URL entity.
 *
 * @see \Drupal\seo_urls\Entity\Question.
 */
class SeoUrlAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    if ($account->hasPermission((string) $this->entityType->getAdminPermission())) {
      return AccessResult::allowed()->cachePerPermissions();
    }
    if ($operation == 'delete' && $entity->isNew()) {
      return AccessResult::forbidden()->addCacheableDependency($entity);
    }

    /** @var \Drupal\seo_urls\Entity\SeoUrlInterface $entity */
    $result = AccessResult::neutral();
    if ($entity->getOwnerId() === $account->id()) {
      $result = $result->orIf(AccessResult::allowedIfHasPermission($account, "{$operation} own {$this->entityTypeId} entities"));
    }
    $result = $result->orIf(AccessResult::allowedIfHasPermission($account, "{$operation} {$this->entityTypeId} entities"));
    $result->cachePerPermissions();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, "add {$this->entityTypeId} entities")->cachePerPermissions();
  }

}
