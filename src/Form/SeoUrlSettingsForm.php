<?php

declare(strict_types=1);

namespace Drupal\seo_urls\Form;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\seo_urls\Entity\SeoUrlInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SEO URL entity settings form.
 *
 * @ingroup seo_urls
 */
class SeoUrlSettingsForm extends ConfigFormBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get(EntityTypeManagerInterface::class);
    return $instance;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'seo_url_entity_type_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'seo_urls.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['info'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('Settings form for SEO URL entities. Manage content types where SEO URLs can be used and field settings here.'),
    ];
    $form['allowed_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed content types'),
      '#description' => $this->t('The content types that are allowed to have SEO URLs.'),
      '#config_target' => 'seo_urls.settings:allowed_content_types',
      '#options' => $this->getAllowedContentTypes(),
    ];
    return $form;
  }

  /**
   * Retrieves the allowed content types.
   *
   * @return array
   *   An array of allowed content types.
   */
  protected function getAllowedContentTypes(): array {
    $types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      // Skip the current entity type.
      if (SeoUrlInterface::ENTITY_TYPE === $entity_type_id) {
        continue;
      }
      /** @var class-string $class */
      $class = $entity_type->getClass();
      $reflection = new \ReflectionClass($class);
      if (!in_array(ContentEntityInterface::class, $reflection->getInterfaceNames())) {
        continue;
      }
      if (!array_key_exists('canonical', $entity_type->getLinkTemplates())) {
        continue;
      }
      $types[$entity_type_id] = $entity_type->getLabel();
    }
    return $types;
  }

}
