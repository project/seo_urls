<?php

declare(strict_types=1);

namespace Drupal\seo_urls\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\seo_urls\Entity\SeoUrlInterface;

/**
 * Form responsible for displaying and applying filters on entity list.
 */
class EntityListFiltersForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_list_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $this->buildFiltersElement($form, $form_state);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * Collect the form elements of collection's filters.
   *
   * @param array $form
   *   The form's render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildFiltersElement(array &$form, FormStateInterface $form_state): void {
    $request = $this->getRequest();

    $form['filters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'entity-list-filter-form--filters',
          'form--inline',
          'clearfix',
        ],
      ],
      SeoUrlInterface::CANONICAL_URL_FIELD => [
        '#type' => 'textfield',
        '#title' => $this->t('Canonical URL'),
        '#default_value' => $request->get(SeoUrlInterface::CANONICAL_URL_FIELD),
      ],
      SeoUrlInterface::SEO_URL_FIELD => [
        '#type' => 'textfield',
        '#title' => $this->t('SEO URL'),
        '#default_value' => $request->get(SeoUrlInterface::SEO_URL_FIELD),
      ],
    ];
  }

  /**
   * Get action buttons of this form.
   *
   * @param array $form
   *   The form's render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function actions(array $form, FormStateInterface $form_state): array {
    $actions = [
      '#type' => 'actions',
      'filter_actions' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['buttons__wrapper'],
        ],
        'filter' => [
          '#type' => 'submit',
          '#value' => $this->t('Filter'),
          '#submit' => [[$this, 'applyFilters']],
          '#weight' => 0,
        ],
      ],
    ];

    $query = $this->getRequest()->query->all();
    unset($query['page']);
    if (!$query) {
      return $actions;
    }

    $actions['filter_actions']['reset'] = [
      '#type' => 'submit',
      '#value' => 'Reset',
      '#submit' => [[$this, 'resetFilters']],
      '#weight' => 5,
    ];

    return $actions;
  }

  /**
   * Submit handler for the "Filter" button.
   *
   * Write the selected filter values in the query string, so they can be used
   * by the entity query on page reload to filter the results.
   *
   * @param array $form
   *   The form's render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function applyFilters(array &$form, FormStateInterface $form_state): void {
    $query = [];

    $this->apply($query, SeoUrlInterface::CANONICAL_URL_FIELD, $form_state);
    $this->apply($query, SeoUrlInterface::SEO_URL_FIELD, $form_state);

    $form_state->setRedirect(
      route_name: '<current>',
      options: ['query' => $query]
    );
  }

  /**
   * Implement certain filter.
   *
   * @param array $query
   *   The query to apply the filter to.
   * @param string $filter_id
   *   The ID of the filter.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function apply(array &$query, string $filter_id, FormStateInterface $form_state): void {
    $value = $form_state->getValue($filter_id);
    if (!$value) {
      return;
    }
    $query[$filter_id] = $value;
  }

  /**
   * Submit handler for the "Reset" button.
   *
   * @param array $form
   *   The form's render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function resetFilters(array &$form, FormStateInterface $form_state): void {
    $form_state->setRedirect('<current>');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {}

}
