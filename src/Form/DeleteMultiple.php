<?php

namespace Drupal\seo_urls\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm as EntityDeleteMultipleForm;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Provides a SEO Url deletion confirmation form.
 *
 * @internal
 */
class DeleteMultiple extends EntityDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletedMessage($count): TranslatableMarkup {
    return $this->formatPlural($count, 'Deleted @count content item.', 'Deleted @count content items.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getInaccessibleMessage($count): TranslatableMarkup {
    return $this->formatPlural($count, "@count content item has not been deleted because you do not have the necessary permissions.", "@count content items have not been deleted because you do not have the necessary permissions.");
  }

}
